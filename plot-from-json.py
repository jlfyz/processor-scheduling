import matplotlib.pyplot as plt

import json
 
f = open('ProcessorScheduling.json')

y = []

name=[]
arrival_time=[]
end_time=[]
start_to_end=[]

fifo,sjf,psjf,rr = [],[],[],[]


data = json.load(f)["testData"]

for proc_data in data:
    y.append(proc_data["numberOfProcesses"])
    fifo.append(proc_data["processManagerData"]["processManagerFIFO"]["avarageWaitTime"])
    sjf.append(proc_data["processManagerData"]["processManagerSJF"]["avarageWaitTime"])
    psjf.append(proc_data["processManagerData"]["processManagerPSJF"]["avarageWaitTime"])
    rr.append(proc_data["processManagerData"]["processManagerRR"]["avarageWaitTime"])
#using the last queue to consider how looks the timeline
data_list = data[-1]["processManagerData"]["queue"]

for proc in data_list:
    name.append(proc['pID'])
    arrival_time.append(proc['arrivalTime'])
    end_time.append(proc['arrivalTime']+proc['burstTime'])
    start_to_end.append(proc['burstTime'])

print(data_list)

fig, (plt1, ganttC) = plt.subplots(2)

plt1.plot(fifo, y, 'd',label='fifo',color='C0')
plt1.plot(sjf, y, 'o',label='sjf', linestyle='--',color='C1', alpha=0.5)
plt1.scatter(psjf, y, label='psjf',color='C2', alpha=0.3)
plt1.scatter(rr, y, label='rr',color='C3', alpha=0.2)

plt1.set_xlabel('Avarage wait time')
plt1.set_ylabel('Number of processes')

plt1.set_title("Avarage wait time/number of processes ratio")
plt1.legend();


plt1.grid(True)

ganttC.barh(name, start_to_end, left=arrival_time)
ganttC.set_xlabel('Timeline')
ganttC.set_ylabel('pID')

ganttC.set_title("Timeline of last process queue in dataset")
ganttC.grid(True);


plt.show()
