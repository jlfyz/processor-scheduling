package ProcessorScheduling.models;

public class ProcessSimulated implements Comparable<ProcessSimulated>{

	private int pID; 
    private int burstTime; 
    private int arrivalTime;
    private int timeLeft;
    private int waitTime;
    private int completionTime;
    private boolean isFinished;
    
    
    public ProcessSimulated(int pID, int burstTime, int arrivalTime)
    {
        this.setpID(pID);
        this.setBurstTime(burstTime);
        this.setArrivalTime(arrivalTime);
        resetProcessSimulated();
    }

	public void resetProcessSimulated() {
		resetTimeLeft();
        resetWaitTime();
        resetCompletionTime();
        this.setFinished(false);
	}
        
	public void resetTimeLeft() {
		this.setTimeLeft(this.burstTime);
	}

	public void resetCompletionTime() {
		this.setCompletionTime(0);
	}

	public void resetWaitTime() {
		this.setWaitTime(0);
	}
    
    public int compareTo(ProcessSimulated that) {
    	return this.getBurstTime() - that.getBurstTime();
    }
    
    public String toString() {
    	String result = "{"
    			+ "\"pID\": " + getpID()+", "
    			+ "\"arrivalTime\": " + getArrivalTime()+", "
    			+ "\"burstTime\": " + getBurstTime()+", "
    			+ "\"waitTime\": " + getWaitTime()+"}";
    	return result;
    }

	public int getBurstTime() {
		return burstTime;
	}

	public void setBurstTime(int burstTime) {
		this.burstTime = burstTime;
	}

	public int getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(int arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public int getTimeLeft() {
		return timeLeft;
	}

	public void setTimeLeft(int timeLeft) {
		this.timeLeft = timeLeft;
	}
	
	public void decTimeLeft() {
		this.timeLeft--;
	}

	public int getpID() {
		return pID;
	}

	public void setpID(int pID) {
		this.pID = pID;
	}

	public int getWaitTime() {
		return waitTime;
	}

	public void setWaitTime(int waitTime) {
		this.waitTime = waitTime;
	}

	public int getCompletionTime() {
		return completionTime;
	}

	public void setCompletionTime(int turnAroundTime) {
		this.completionTime = turnAroundTime;
	}

	public boolean isFinished() {
		return isFinished;
	}

	public void setFinished(boolean isFinished) {
		this.isFinished = isFinished;
	}
}