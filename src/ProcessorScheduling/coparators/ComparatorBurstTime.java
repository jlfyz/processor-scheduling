package ProcessorScheduling.coparators;

import java.util.Comparator;

import ProcessorScheduling.models.ProcessSimulated;

public class ComparatorBurstTime implements Comparator<ProcessSimulated>{
    @Override
    public int compare(ProcessSimulated o1, ProcessSimulated o2) {
        int result =  o1.getBurstTime() - o2.getBurstTime();
        if(result != 0) {
        	return result;
        }
        return o1.getBurstTime() - o2.getBurstTime();
    }
}