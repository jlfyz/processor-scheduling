package ProcessorScheduling.coparators;

import java.util.Comparator;

import ProcessorScheduling.models.ProcessSimulated;

public class ComparatorArrivalAndBT implements Comparator<ProcessSimulated>{
    @Override
    public int compare(ProcessSimulated o1, ProcessSimulated o2) {
    	int result =  o1.getArrivalTime() - o2.getArrivalTime();
        if (result != 0) {
            return result;
        }
        return o1.getBurstTime() - o2.getBurstTime();
    }
}
