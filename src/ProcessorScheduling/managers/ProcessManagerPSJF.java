package ProcessorScheduling.managers;

import java.util.ArrayList;
import java.util.Collections;


import ProcessorScheduling.coparators.ComparatorArrivalAndBT;
import ProcessorScheduling.coparators.ComparatorShortestDeltaTime;
import ProcessorScheduling.models.ProcessSimulated;

public class ProcessManagerPSJF extends ProcessManager{
	protected ArrayList<ProcessSimulated> processes = new ArrayList<ProcessSimulated>();
	
	public ProcessManagerPSJF(ArrayList<ProcessSimulated> processes) {
		super(processes);
		this.processes = processes;
	}
	
	public void add(ProcessSimulated proc) {
		this.processes.add(proc);
		Collections.sort(processes, new ComparatorArrivalAndBT());
	}
	
	public double estimateAvarageWaitTime() {
		Collections.sort(this.processes, new ComparatorShortestDeltaTime());
		this.processes = setWaitTime(this.processes);
		Long sum = this.processes.stream().mapToLong(x -> x.getWaitTime()).sum();
		return (double) sum / (double) this.processes.size();
	}

	public ArrayList<ProcessSimulated> setWaitTime(ArrayList<ProcessSimulated> processes) {
		int processNum = processes.size();
		
		int completedProcesses = 0, currentTime = 0;
		
		int finishTimeOfCurrentProcess;
		
		ProcessSimulated shortestProcess = null;
		boolean isFound = false;		

		int minNonZeroProcTime = Integer.MAX_VALUE;
		
		while(completedProcesses != processNum) {
			//min non-zero time lap
			for(ProcessSimulated pr: processes) {
				if(isMinDeltaT(currentTime, minNonZeroProcTime, pr)) {
					minNonZeroProcTime = pr.getTimeLeft();
					shortestProcess = pr;
					isFound = true;
				}
			}
			
			//not found = skip
			if(!isFound) {
				currentTime++;
				continue;
			}
			
			shortestProcess.decTimeLeft();
			minNonZeroProcTime = shortestProcess.getTimeLeft();
			
			if(shortestProcess.getTimeLeft() == 0) {
				completedProcesses++;
				
				finishTimeOfCurrentProcess = currentTime + 1;
				shortestProcess.setWaitTime(finishTimeOfCurrentProcess - shortestProcess.getBurstTime());
				//reset for next while-loop
				minNonZeroProcTime = Integer.MAX_VALUE;
				isFound = false;
				
				if(shortestProcess.getWaitTime() < 0) {
					shortestProcess.setWaitTime(0);
				}
			}
			currentTime++;
		}
		return processes;
	}

	public boolean isMinDeltaT(int currentTime, int minProc, ProcessSimulated pr) {
		return (pr.getArrivalTime() <= currentTime) && (pr.getTimeLeft() < minProc) && (pr.getTimeLeft() > 0);
	}
	
	public void setTurnAroundTime() {
		for(ProcessSimulated pr: this.processes) {
			pr.setCompletionTime(pr.getBurstTime() + pr.getWaitTime());
		}
	}
	
	public String algoName() {
		return "PSJF";
	}

	public String toString() {
		String res = "\"processManagerPSJF\": {\"algorithmName\": \"Preemptive shortest job first\", \"avarageWaitTime\": "+ this.estimateAvarageWaitTime()+"}";
		for(ProcessSimulated pr: processes){
			pr.resetProcessSimulated();
		}
		return res;
	}
}
