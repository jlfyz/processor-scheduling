package ProcessorScheduling.managers;

import java.util.ArrayList;
import java.util.Collections;

import ProcessorScheduling.coparators.ComparatorArrivalAndBT;
import ProcessorScheduling.models.ProcessSimulated;

public class ProcessManagerSJF extends ProcessManager {
	protected ArrayList<ProcessSimulated> processes = new ArrayList<ProcessSimulated>();
	
	public ProcessManagerSJF(ArrayList<ProcessSimulated> processes) {
		super(processes);
		this.processes = processes;
	}
	
	public void add(ProcessSimulated proc) {
		this.processes.add(proc);
		Collections.sort(processes, new ComparatorArrivalAndBT());
	}
	
	public double estimateAvarageWaitTime() {
		Collections.sort(this.processes, new ComparatorArrivalAndBT());
		double time = 0.0;
		for(int i = 0; i < this.processes.size(); i++) {
			if(i < this.processes.size() - 1)
				this.processes.get(i + 1).setWaitTime((int) (this.processes.get(i).getWaitTime() + (this.processes.get(i).getBurstTime())));
			time += this.processes.get(i).getWaitTime();
		}
		System.out.println((double)time/(double)this.processes.size());
		return (double)time/(double)this.processes.size();
	}
	
	public String algoName() {
		return "SJF";
	}

	public String toString() {
		String res ="\"processManagerSJF\": {\"algorithmName\": \"Shortest job first\", \"avarageWaitTime\": "+ this.estimateAvarageWaitTime()+"}";
		for(ProcessSimulated pr: processes){
			pr.resetProcessSimulated();
		}
		return res;
	}
}
