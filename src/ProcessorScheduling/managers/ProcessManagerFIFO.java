package ProcessorScheduling.managers;

import java.util.ArrayList;
import java.util.Collections;

import ProcessorScheduling.coparators.ComparatorTimeArrival;
import ProcessorScheduling.models.ProcessSimulated;

public class ProcessManagerFIFO extends ProcessManager {
	protected ArrayList<ProcessSimulated> processes = new ArrayList<ProcessSimulated>();
	
	public ProcessManagerFIFO(ArrayList<ProcessSimulated> processes) {
		super(processes);	
		this.processes = processes;
	}
	
	public ProcessSimulated peek() {
		if (this.processes == null) {
			return null;
		}
		return this.processes.get(0);
	}
	
	public ProcessSimulated poll() {
		if (this.processes == null) {
			return null;
		}
		ProcessSimulated answer = this.processes.get(0);
		this.processes.remove(0);
		return answer;
	}
	
	public double estimateAvarageWaitTime() {
		Collections.sort(this.processes, new ComparatorTimeArrival());
		double time = 0.0;
		this.processes.get(0).setCompletionTime(this.processes.get(0).getBurstTime());
		for(int i = 0; i < this.processes.size(); i++) {
			if(i < this.processes.size() - 1)
			{
				this.processes.get(i + 1).setWaitTime((this.processes.get(i).getWaitTime() + (this.processes.get(i).getBurstTime())));
				this.processes.get(i + 1).setCompletionTime((this.processes.get(i).getCompletionTime() + this.processes.get(i+1).getBurstTime()));	
			}
			time += this.processes.get(i).getWaitTime();
		}
		System.out.println((double)time/(double)this.processes.size());
		return (double)time/(double)this.processes.size();
	}
	
	public String algoName() {
		return "FiFo";
	}
	
	public String toString() {
		String res = "\"processManagerFIFO\": {\"algorithmName\": \"First in first out\", \"avarageWaitTime\": "+ this.estimateAvarageWaitTime()+"}";
		for(ProcessSimulated pr: processes){
			pr.resetProcessSimulated();
		}
		return res;
	}
}
