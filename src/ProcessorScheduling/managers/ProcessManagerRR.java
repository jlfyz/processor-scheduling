package ProcessorScheduling.managers;

import java.util.ArrayList;
import java.util.Collections;

import ProcessorScheduling.coparators.ComparatorBurstTime;
import ProcessorScheduling.coparators.ComparatorShortestDeltaTime;
import ProcessorScheduling.coparators.ComparatorTimeArrival;
import ProcessorScheduling.models.ProcessSimulated;

public class ProcessManagerRR extends ProcessManager {
	protected ArrayList<ProcessSimulated> processes = new ArrayList<ProcessSimulated>();
	
	public ProcessManagerRR(ArrayList<ProcessSimulated> processes) {
		super(processes);
		this.processes = processes;
	}
	
	public void add(ProcessSimulated proc) {
		this.processes.add(proc);
		Collections.sort(processes, new ComparatorTimeArrival());
	}
	
	public double estimateAvarageWaitTime() {
		Collections.sort(this.processes, new ComparatorShortestDeltaTime());
		this.processes = setWaitTime(this.processes);
		Long sum = this.processes.stream().mapToLong(x -> x.getWaitTime()).sum();
		return (double) sum / (double) this.processes.size();
	}

	public ArrayList<ProcessSimulated> setWaitTime(ArrayList<ProcessSimulated> processes) {
		int currentTime = 0;
		boolean isDone = true;
		int dT = 0;
		
		while(true) {
			isDone = true;
			for(ProcessSimulated pr: processes) {
				dT = findDT(currentTime, processes);
				if(pr.getTimeLeft() > 0) {
					isDone = false;
					if(pr.getTimeLeft() > dT) {
						currentTime += dT;
						pr.setTimeLeft(pr.getTimeLeft() - dT);
					} else {
						currentTime += pr.getTimeLeft();
						pr.setCompletionTime(currentTime);
						pr.setWaitTime(currentTime - (pr.getBurstTime() - pr.getArrivalTime()));
						pr.setTimeLeft(0);
						pr.setFinished(true);
					}
				}
			}
			if (isDone == true) {
				break;
			}
		}		
		return processes;
	}

	public int findDT(int currentTime, ArrayList<ProcessSimulated> processes) {
		return processes.stream()
				.filter(o -> o.getArrivalTime() <= currentTime && !o.isFinished())
				.min(new ComparatorBurstTime()).orElse(new ProcessSimulated(0, 2, 1)).getBurstTime();
	}
	
	public boolean isMinDeltaT(int currentTime, int minProc, ProcessSimulated pr) {
		return (pr.getArrivalTime() <= currentTime) && (pr.getTimeLeft() < minProc) && (pr.getTimeLeft() > 0);
	}

	public String algoName() {
		return "RR";
	}
	
	public String toString() {
		String res =  "\"processManagerRR\": {\"algorithmName\": \"Round robin\", \"avarageWaitTime\": "+ this.estimateAvarageWaitTime()+"}";
		for(ProcessSimulated pr: processes){
			pr.resetProcessSimulated();
		}
		return res;
	}
}
