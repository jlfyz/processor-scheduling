package ProcessorScheduling;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import ProcessorScheduling.managers.ProcessManager;
import ProcessorScheduling.managers.ProcessManagerFIFO;
import ProcessorScheduling.managers.ProcessManagerPSJF;
import ProcessorScheduling.managers.ProcessManagerRR;
import ProcessorScheduling.managers.ProcessManagerSJF;
import ProcessorScheduling.models.ProcessSimulated;


public class mainTestingClass {
	protected static ProcessManager[] processManagers = new ProcessManager[4];
	
//	Hints to the topic 1: 
//	- algorithms are best checked for the same test data (i.e., the same test strings of reporting processes) 
//	- the queue should have at least 50 - 100 processes; it is worthwhile to be able to determine the number of processes in the queue;
	//the result will be the average values of waiting times. 
//	- in each sequence (queue) should be N processes with random processor phase lengths 
	//(the distribution of phase lengths should be chosen so that it corresponds to the situation in the real system, 
	//where it is not uniform), reporting at random moments (choose the parameters so that it could form a queue of
	//processes waiting for processor allocation).
//	- Possible representation of the process: a record (number, length of the processor phase, moment of the request, \
	//waiting time /initially equal to 0/...) 
//	It would be nice to be able to control simulation parameters (e.g. number of processes, what else ?).
	
	public static void main(String[] args) {
		StringBuilder res = new StringBuilder();//+"}";
		res.append("{\"testData\":[");
		res.append(amountOfTests(50, 1, 5, 10));
		res.append("]}");
		File fl = new File("ProcessorScheduling.json");
		try (BufferedWriter writer = new BufferedWriter(new FileWriter(fl))) {
		    writer.write(res.toString());
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	private static StringBuilder amountOfTests(int countOfTests, int phase, int burstTimeCoefficient, int arrivalTimeCoefficient) {
		StringBuilder sb = new StringBuilder();
		for(int i = 1; i <= countOfTests; i++) {
			System.out.println("For " + phase * i + " processes:");
			if(i == countOfTests)
				sb.append("{\"numberOfProcesses\": " + phase * i + ", \"processManagerData\": {"+testForExactProcessCount(phase * i, burstTimeCoefficient, arrivalTimeCoefficient)+"}}");
			else {
				sb.append("{\"numberOfProcesses\": " + phase * i + ", \"processManagerData\": {"+testForExactProcessCount(phase * i, burstTimeCoefficient, arrivalTimeCoefficient)+"}},");	
			}
			System.out.println("-------------");	
		}
		return sb;
	}
	
	private static StringBuilder testForExactProcessCount(int ProcessCount, int burstTimeCoefficient, int arrivalTimeCoefficient) {
		ArrayList<ProcessSimulated> processes = new ArrayList<ProcessSimulated>();
		for(int i = 0; i < ProcessCount; i++) {
			processes.add(new ProcessSimulated(
					i, 
					1 + (int) (Math.random() * ProcessCount/burstTimeCoefficient),
					i + 1 + (int) (Math.random() * ProcessCount/arrivalTimeCoefficient)));
		}
		StringBuilder result = new StringBuilder();
		result.append( "\"queue\": " + processes.toString()+",");
		processManagers[0] = new ProcessManagerFIFO(processes);
		processManagers[1] = new ProcessManagerRR(processes);
		processManagers[2] = new ProcessManagerPSJF(processes);
		processManagers[3] = new ProcessManagerSJF(processes);
		int counter = 0;
		for(ProcessManager pm: processManagers) {
			if(counter != 3) {
				result.append(pm + ",");			
			} else {
				result.append(pm);		
			}
			counter++;
		}
		return result;
	}
}
